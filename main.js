let etcd
let etcdWatcher
let fnStartWatcher
let healthCheckInterval
let lastHealthPing = Date.now()
let lastKnownIndex
let restartIfUnhealthyForMs = 6000
const TimeBuckets = require('time-buckets')
const process = require('process')

const tb = new TimeBuckets({
  maxAge: 5 * 60 * 1000,
  maxBuckets: 10
});


const self = {
  startWatcher: function(etcdObj, startWatcherFn) {
    if(etcdObj) {
      etcd = etcdObj;
      fnStartWatcher = startWatcherFn;
    }
    
    if(!fnStartWatcher) {
      console.error('Cannot start etcd watcher health because startWatcher function is not defined');
      return;
    }

    etcdWatcher = fnStartWatcher(lastKnownIndex);

    if(!etcd) {
      console.error('Cannot start etcd watcher health because etcd object is not defined');
      return;
    }

    self.startMonitoring();
  },


  startMonitoring: function() {
    console.log('> Starting etcd health monitoring');
    
    // clear intervals
    if(healthCheckInterval) {
      clearInterval(healthCheckInterval);
    }

    lastHealthPing = Date.now();
    lastKnownIndex = 0;

    // check if healthy
    healthCheckInterval = setInterval(function() {
      self.isHealthy()
      .then(function(isHealthy) {
        if(!isHealthy && 
          (Date.now() - lastHealthPing) >= restartIfUnhealthyForMs
        ) {
          self.restartWatcher();
        }
      })
      .catch(function(err) {
        if(err) {
          console.trace(err);
        }
      });
    }, restartIfUnhealthyForMs);
  },


  restartWatcher: function() {
    console.log('> Restarting etcd watcher - not healthy for ' + 
      (Math.floor((Date.now() - lastHealthPing) / 100) / 10) + 's ' +
      'last known index: ' + lastKnownIndex
    );
    
    tb.observe('restartWatcher', 1);
    let restarts = tb.getValues('restartWatcher');

    if(restarts.count > 5) {
      console.error('Etcd watcher not healthy. Restarted ' + restarts.count + ' times in the last 5mins');

      if(restarts.count > 15) {
        console.error(`Exiting process. Too many etcd watcher restarts.`)
        process.exit(1)
      }
    }

    etcdWatcher.stop();
    self.startWatcher();
  },


  updateLastHealthPingTime: function(data, headers) {
    lastHealthPing = Date.now();
    if(data.node && data.node.modifiedIndex) {
      lastKnownIndex = data.node.modifiedIndex;
    }
  },


  isHealthy: function() {
    return new Promise(function(resolve, reject) {
      var timeDiff = Date.now() - lastHealthPing;
      
      if(timeDiff < restartIfUnhealthyForMs) {
        resolve(true);
      } else {
        etcd.get('/', function(err, data, headers) {
          if(err) {
            console.trace(err);
            resolve(false);
            return;
          }

          var etcdIndex = parseInt(headers['x-etcd-index'], 10);
          if(!lastKnownIndex) {
            lastKnownIndex = etcdIndex;
            resolve(true);
            return;
          }
          
          if(etcdIndex <= lastKnownIndex + 1) {
            lastHealthPing = Date.now();
            resolve(true);
            return;
          }
          
          console.log(
            'Unhealthy etcd watch index.', 
            'Last known:', lastKnownIndex, 
            'Last etcd index:', etcdIndex
          );
          resolve(false);
        });
      }
    });
  },

  setRestartLimit: restartLimitMs => {
    restartIfUnhealthyForMs = parseInt(restartLimitMs, 10);
  }
}


module.exports = self;